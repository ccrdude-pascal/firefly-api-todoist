{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyAPITodoist;

{$warn 5023 off : no warning about unused units}
interface

uses
  Firefly.Todoist.ProjectPicker, Firefly.Todoist.UI, Firefly.Todoist.V2, 
  Firefly.Todoist.ControlTaskList, Firefly.Todoist.ControlSourcePicker, 
  Firefly.Todoist.FrameTask, Firefly.Todoist.DialogTaskSourcePicker, 
  Firefly.Todoist.ControlCommentPanel, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('Firefly.Todoist.ControlTaskList', 
    @Firefly.Todoist.ControlTaskList.Register);
  RegisterUnit('Firefly.Todoist.ControlSourcePicker', 
    @Firefly.Todoist.ControlSourcePicker.Register);
end;

initialization
  RegisterPackage('FireflyAPITodoist', @Register);
end.
