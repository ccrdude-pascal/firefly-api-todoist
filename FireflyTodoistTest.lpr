program FireflyTodoistTest;

{$mode objfpc}{$H+}

uses
   {$IFDEF UNIX}
   cthreads,
   {$ENDIF}
   {$IFDEF HASAMIGA}
   athreads,
   {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms,
   FireflyTodoistTest.FormMain,
   Layers.JSON.FreePascal,
   Layers.Transport.Synapse;

   {$R *.res}

begin
   RequireDerivedFormResource := True;
   Application.Scaled := True;
   Application.Initialize;
   Application.CreateForm(TFormFireflyTodoistAPITest, FormFireflyTodoistAPITest
      );
   Application.Run;
end.
