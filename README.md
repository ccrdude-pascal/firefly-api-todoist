# Firefly Todoist API

This is a FreePascal interface to the [Todoist REST API](https://developer.todoist.com/rest/v2/#overview).

Todoist is a task handling project management tool.

## Status

Work In Progress - basic functionality like listing projects, labels, tasks and comments works. Details like comment attachments are still missing.

## Dependencies

This package uses the following other packages:
* FreePascal version of at least 3.3.1 because custom attributes are used.
* [Firefly Core](https://gitlab.com/ccrdude-pascal/firefly-core)
* [Firefly Layers](https://gitlab.com/ccrdude-pascal/firefly-layers) for
  abstract layers to acess the Internet and work with JSON files, allowing to
  extend the library to e.g. Delphi, where other libraries for that purpose
  might be available.
* [Firefly Object Visualization Mapping](https://gitlab.com/ccrdude-pascal/firefly-ovm)
  to simplify display of API lists / objects.
