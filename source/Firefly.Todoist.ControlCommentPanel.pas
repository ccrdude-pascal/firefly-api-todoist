unit Firefly.Todoist.ControlCommentPanel;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Controls,
   StdCtrls,
   ExtCtrls,
   Firefly.Todoist.V2;

type

   { TTodoistCommentPanel }

   { TCommentPanel }

   TCommentPanel = class(TPanel)
   private
      FLabelComment: TLabel;
      FLabelCreated: TLabel;
      FLabelAuthor: TLabel;
      FPanelMeta: TFlowPanel;
      function GetAuthor: string;
      function GetContent: string;
      function GetTimeStamp: string;
      procedure SetAuthor(AValue: string);
      procedure SetContent(AValue: string);
      procedure SetTimeStamp(AValue: string);
   public
      constructor Create(TheOwner: TComponent); override;
   published
      property Author: string read GetAuthor write SetAuthor;
      property Content: string read GetContent write SetContent;
      property TimeStamp: string read GetTimeStamp write SetTimeStamp;
   end;

   TTodoistCommentPanel = class(TCommentPanel)
   private
      procedure SetComment(AValue: TTodoistComment);
   public
      property Comment: TTodoistComment write SetComment;
   end;

implementation

uses
   Graphics;

{ TCommentPanel }

function TCommentPanel.GetAuthor: string;
begin
   Result := FLabelAuthor.Caption;
end;

function TCommentPanel.GetContent: string;
begin
   Result := FLabelComment.Caption;
end;

function TCommentPanel.GetTimeStamp: string;
begin
   Result := FLabelCreated.Caption;
end;

procedure TCommentPanel.SetAuthor(AValue: string);
begin
   FLabelAuthor.Caption := AValue;
end;

procedure TCommentPanel.SetContent(AValue: string);
begin
   FLabelComment.Caption := AValue;
end;

procedure TCommentPanel.SetTimeStamp(AValue: string);
begin
   FLabelCreated.Caption := AValue;
end;

constructor TCommentPanel.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   Self.AutoSize := True;
   Self.BevelInner := bvNone;
   Self.BevelOuter := bvNone;
   Self.ChildSizing.LeftRightSpacing := 6;
   Self.ChildSizing.HorizontalSpacing := 6;
   Self.ChildSizing.TopBottomSpacing := 6;
   Self.ChildSizing.VerticalSpacing := 6;
   FLabelComment := TLabel.Create(Self);
   FLabelComment.Parent := Self;
   FLabelComment.Caption := '';
   FLabelComment.WordWrap := True;
   FLabelComment.Align := alTop;
   FPanelMeta := TFlowPanel.Create(Self);
   FPanelMeta.Parent := Self;
   FPanelMeta.Align := alTop;
   FPanelMeta.AutoSize := True;
   FPanelMeta.BevelInner := bvNone;
   FPanelMeta.BevelOuter := bvNone;
   FPanelMeta.ChildSizing.LeftRightSpacing := 6;
   FLabelAuthor := TLabel.Create(Self);
   FLabelAuthor.Parent := FPanelMeta;
   FLabelAuthor.Caption := '';
   FLabelAuthor.Font.Style := [fsBold];
   FLabelAuthor.AutoSize := True;
   FLabelAuthor.Anchors := [];
   FLabelAuthor.Align := alNone;
   FLabelAuthor.BorderSpacing.Right := 6;
   FLabelCreated := TLabel.Create(Self);
   FLabelCreated.Parent := FPanelMeta;
   FLabelCreated.Caption := '';
   FLabelCreated.AutoSize := True;
   FLabelCreated.Anchors := [];
   FLabelCreated.Align := alNone;
end;

procedure TTodoistCommentPanel.SetComment(AValue: TTodoistComment);
begin
   Self.Author := 'Myself';
   if Assigned(AValue) then begin
      Self.Content := AValue.Content;
      Self.TimeStamp := AValue.PostedAt;
   end else begin
      Self.Content := '?';
      Self.TimeStamp := '?';
   end;
end;

end.
