unit Firefly.Todoist.ControlSourcePicker;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   StdCtrls,
   ComCtrls,
   ExtCtrls,
   OVM.ComboBox,
   OVM.ComboBoxEx,
   OVM.TreeView,
   OVM.ListView,
   Firefly.Todoist.V2,
   Firefly.Todoist.ControlTaskList;

type
   TOnTodoistSelect = procedure(AnTodoistObject: TTodoistContainerObject; Close: boolean) of object;

   { TFireflyTodoistSourcePicker }

   TFireflyTodoistSourcePicker = class(TCustomControl)
   private
      FAPI: TTodoistAPIv2;
      FListView: TListView;
      FOnSelect: TOnTodoistSelect;
      FPageControl: TPageControl;
      FTabLabels: TTabSheet;
      FListLabels: TListView;
      FTabProjects: TTabSheet;
      FTaskView: TFireflyTodoistTaskList;
      FTreeProjects: TTreeView;
      FListSections: TListView;
      procedure SetAPI(AValue: TTodoistAPIv2);
      procedure ListLabelsSelect(Sender: TObject; Item: TListItem; Selected: boolean);
      procedure ListLabelsDblClick(Sender: TObject);
      procedure ListSectionsSelect(Sender: TObject; Item: TListItem; Selected: boolean);
      procedure ListSectionsDblClick(Sender: TObject);
      procedure TreeProjectsSelectionChanged(Sender: TObject);
      procedure TreeProjectsDblClick(Sender: TObject);
   protected
      procedure FillDemoData; virtual;
      procedure RefreshProjects; virtual;
      procedure RefreshLabels; virtual;
      procedure TriggerLabelSelect(ALabel: TTodoistLabel; AClose: boolean); virtual;
      procedure TriggerProjectSelect(AProject: TTodoistProject; AClose: boolean); virtual;
      procedure TriggerSectionSelect(ASection: TTodoistSection; AClose: boolean); virtual;
      procedure TasksToListView(ATasks: TTodoistTasks; AListView: TListView); virtual;
   public
      constructor Create(AOwner: TComponent); override;
      property API: TTodoistAPIv2 read FAPI write SetAPI;
   published
      property Align;
      property Anchors;
      property BorderSpacing;
      property Constraints;
      property Visible;
      property TaskView: TFireflyTodoistTaskList read FTaskView write FTaskView;
      property ListView: TListView read FListView write FListView;
      property OnSelect: TOnTodoistSelect read FOnSelect write FOnSelect;
   end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('Firefly', [TFireflyTodoistSourcePicker]);
end;

{ TFireflyTodoistSourcePicker }

procedure TFireflyTodoistSourcePicker.SetAPI(AValue: TTodoistAPIv2);
begin
   FAPI := AValue;
   RefreshProjects;
   RefreshLabels;
end;

procedure TFireflyTodoistSourcePicker.FillDemoData;
begin

end;

procedure TFireflyTodoistSourcePicker.RefreshProjects;
begin
   if not Assigned(FAPI) then begin
      Exit;
   end;
   FTreeProjects.DisplayItems<TTodoistProject>(FAPI.Projects);
   FTreeProjects.FullExpand;
end;

procedure TFireflyTodoistSourcePicker.RefreshLabels;
var
   i: integer;
begin
   if not Assigned(FAPI) then begin
      Exit;
   end;
   FListLabels.DisplayItems<TTodoistLabel>(FAPI.Labels);
   if (FListLabels.Columns.Count > 0) then begin
      //FListLabels.Columns[0].Visible := False;
      for i := 2 to Pred(FListLabels.Columns.Count) do begin
         FListLabels.Columns[i].Visible := False;
      end;
   end;
end;

procedure TFireflyTodoistSourcePicker.TriggerLabelSelect(ALabel: TTodoistLabel; AClose: boolean);
begin
   if Assigned(FTaskView) then begin
      FTaskView.Tasks := ALabel.Tasks;
   end;
   if Assigned(FListView) then begin
      TasksToListView(ALabel.Tasks, FListView);
   end;
   if Assigned(FOnSelect) then begin
      FOnSelect(ALabel, AClose);
   end;
end;

procedure TFireflyTodoistSourcePicker.TriggerProjectSelect(AProject: TTodoistProject; AClose: boolean);
begin
   if Assigned(FTaskView) then begin
      FTaskView.Tasks := AProject.Tasks;
   end;
   if Assigned(FListView) then begin
      TasksToListView(AProject.Tasks, FListView);
   end;
   if Assigned(FOnSelect) then begin
      FOnSelect(AProject, AClose);
   end;
end;

procedure TFireflyTodoistSourcePicker.TriggerSectionSelect(ASection: TTodoistSection; AClose: boolean);
begin
   if Assigned(FTaskView) then begin
      FTaskView.Tasks := ASection.Tasks;
   end;
   if Assigned(FListView) then begin
      TasksToListView(ASection.Tasks, FListView);
   end;
   if Assigned(FOnSelect) then begin
      FOnSelect(ASection, AClose);
   end;
end;

procedure TFireflyTodoistSourcePicker.TasksToListView(ATasks: TTodoistTasks; AListView: TListView);
begin
   AListView.DisplayItems<TTodoistTask>(ATasks);
end;

procedure TFireflyTodoistSourcePicker.ListLabelsSelect(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   if Selected then begin
      TriggerLabelSelect(TTodoistLabel(Item.Data), False);
   end;
end;

procedure TFireflyTodoistSourcePicker.ListSectionsSelect(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   if Selected then begin
      FTreeProjects.Selected := nil;
   end;
end;

procedure TFireflyTodoistSourcePicker.ListSectionsDblClick(Sender: TObject);
begin
   if not Assigned(FListSections.Selected) then begin
      Exit;
   end;
   if not Assigned(FListSections.Selected.Data) then begin
      Exit;
   end;
   TriggerSectionSelect(TTodoistSection(FListSections.Selected.Data), True);
end;

procedure TFireflyTodoistSourcePicker.ListLabelsDblClick(Sender: TObject);
begin
   if not Assigned(FListLabels.Selected) then begin
      Exit;
   end;
   if not Assigned(FListLabels.Selected.Data) then begin
      Exit;
   end;
   TriggerLabelSelect(TTodoistLabel(FListLabels.Selected.Data), True);
end;

procedure TFireflyTodoistSourcePicker.TreeProjectsSelectionChanged(Sender: TObject);
var
   p: TTodoistProject;
begin
   if not Assigned(FTreeProjects.Selected) then begin
      Exit;
   end;
   if not Assigned(FTreeProjects.Selected.Data) then begin
      Exit;
   end;
   p := TTodoistProject(FTreeProjects.Selected.Data);
   TriggerProjectSelect(p, False);
   FListSections.Selected := nil;
   FListSections.DisplayItems<TTodoistSection>(p.Sections);
end;

procedure TFireflyTodoistSourcePicker.TreeProjectsDblClick(Sender: TObject);
begin
   if not Assigned(FTreeProjects.Selected) then begin
      Exit;
   end;
   if not Assigned(FTreeProjects.Selected.Data) then begin
      Exit;
   end;
   TriggerProjectSelect(TTodoistProject(FTreeProjects.Selected.Data), True);
end;

constructor TFireflyTodoistSourcePicker.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FPageControl := TPageControl.Create(Self);
   FPageControl.Name := 'PageControl';
   FPageControl.Parent := Self;
   FPageControl.ControlStyle := FPageControl.ControlStyle + [csNoDesignSelectable];
   FPageControl.SetSubComponent(True);
   FPageControl.Align := alClient;
   FTabProjects := FPageControl.AddTabSheet;
   FTabProjects.Caption := 'Projects';
   FTreeProjects := TTreeView.Create(Self);
   FTreeProjects.Parent := FTabProjects;
   FTreeProjects.Align := alClient;
   FTreeProjects.BorderStyle := bsNone;
   FTreeProjects.ReadOnly := True;
   FTreeProjects.RowSelect := True;
   FTreeProjects.HideSelection := False;
   FTreeProjects.OnSelectionChanged := TreeProjectsSelectionChanged;
   FTreeProjects.OnDblClick := TreeProjectsDblClick;
   FListSections := TListView.Create(Self);
   FListSections.Parent := FTabProjects;
   FListSections.Align := alRight;
   FListSections.Width := 200;
   FListSections.BorderStyle := bsNone;
   FListSections.ReadOnly := True;
   FListSections.RowSelect := True;
   FListSections.ViewStyle := ComCtrls.vsList;
   FListSections.HideSelection := False;
   FListSections.ShowColumnHeaders := False;
   FListSections.OnSelectItem := ListSectionsSelect;
   FListSections.OnDblClick := ListSectionsDblClick;
   FTabLabels := FPageControl.AddTabSheet;
   FTabLabels.Caption := 'Labels';
   FListLabels := TListView.Create(Self);
   FListLabels.Parent := FTabLabels;
   FListLabels.Align := alClient;
   FListLabels.BorderStyle := bsNone;
   FListLabels.ViewStyle := ComCtrls.vsReport;
   FListLabels.RowSelect := True;
   FListLabels.ReadOnly := True;
   FListLabels.ShowColumnHeaders := False;
   FListLabels.OnSelectItem := ListLabelsSelect;
   FListLabels.OnDblClick := ListLabelsDblClick;
   if (csDesigning in AOwner.ComponentState) then begin
      FillDemoData;
   end;
   Self.Align := alLeft;
   Self.Width := 200;
end;

end.
