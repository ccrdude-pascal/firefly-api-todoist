unit Firefly.Todoist.ControlTaskList;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Firefly.Todoist.V2,
   Firefly.Todoist.FrameTask;

type

   { TFireflyTodoistTaskList }

   TFireflyTodoistTaskList = class(TCustomControl)
   private
      FScrollBox: TScrollBox;
      FTasks: TTodoistTasks;
      procedure SetTasks(AValue: TTodoistTasks);
   protected
      procedure ClearItems;
      procedure Refresh; virtual;
   public
      constructor Create(AOwner: TComponent); override;
      property Tasks: TTodoistTasks read FTasks write SetTasks;
   published
      property Align;
      property Anchors;
      property BorderSpacing;
      property Constraints;
   end;

procedure Register;

implementation

procedure Register;
begin
   RegisterComponents('Firefly', [TFireflyTodoistTaskList]);
end;

{ TFireflyTodoistTaskList }

procedure TFireflyTodoistTaskList.SetTasks(AValue: TTodoistTasks);
begin
   FTasks := AValue;
   Refresh;
end;

procedure TFireflyTodoistTaskList.ClearItems;
var
   f: TFrameTodoistTask;
   i: integer;
begin
   for i := Pred(FScrollBox.ControlCount) downto 0 do begin
      if (FScrollBox.Controls[i] is TFrameTodoistTask) then begin
         f := TFrameTodoistTask(FScrollBox.Controls[i]);
         FScrollBox.RemoveControl(f);
         f.Free;
      end;
   end;
end;

procedure TFireflyTodoistTaskList.Refresh;
var
   f: TFrameTodoistTask;
   t: TTodoistTask;
begin
   FScrollBox.DisableAlign;
   try
      ClearItems;
      if not Assigned(FTasks) then begin
         Exit;
      end;
      for t in FTasks do begin
         f := TFrameTodoistTask.Create(nil);
         f.Parent := FScrollBox;
         f.Align := alTop;
         f.Task := t;
      end;
   finally
      FScrollBox.EnableAlign;
   end;
end;

constructor TFireflyTodoistTaskList.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FScrollBox := TScrollBox.Create(Self);
   FScrollBox.Name := 'Scrollbox';
   FScrollBox.Parent := Self;
   FScrollBox.Align := alClient;
   FScrollBox.ControlStyle := FScrollBox.ControlStyle + [csNoDesignSelectable];
   FScrollBox.BorderStyle := bsNone;
   Self.Align := alClient;
end;

end.
