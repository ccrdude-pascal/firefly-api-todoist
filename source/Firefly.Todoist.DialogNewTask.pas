unit Firefly.Todoist.DialogNewTask;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ButtonPanel,
   ExtCtrls,
   StdCtrls,
   ComboEx,
   Layers.JSON.Base,
   OVM.Basics,
   OVM.ComboBox,
   OVM.ComboBoxEx,
   Firefly.Todoist.V2;

type

   { TFormDialogTodoistNewTask }

   TFormDialogTodoistNewTask = class(TForm)
      panelButtons: TButtonPanel;
      cbProjects: TComboBoxEx;
      cbSections: TComboBoxEx;
      cbPriority: TComboBoxEx;
      editTitle: TEdit;
      labelProject: TLabel;
      labelSection: TLabel;
      labelTitle: TLabel;
      labelDescription: TLabel;
      labelPriority: TLabel;
      memoDescription: TMemo;
      panelDetails: TPanel;
      procedure cbProjectsSelect(Sender: TObject);
      procedure editTitleChange(Sender: TObject);
   private
      procedure TestTaskReady;
   public
      function Execute(const AnAPI: TTodoistAPIv2; out TheDetails: TLayeredJSONObject; ADefaultProjectID: string = ''): boolean;
   end;

function ShowTodoistNewTaskDialog(const AnAPI: TTodoistAPIv2; out TheDetails: TLayeredJSONObject; ADefaultProjectID: string = ''): boolean;

implementation

{$R *.lfm}

function ShowTodoistNewTaskDialog(const AnAPI: TTodoistAPIv2; out TheDetails: TLayeredJSONObject; ADefaultProjectID: string): boolean;
var
   form: TFormDialogTodoistNewTask;
begin
   form := TFormDialogTodoistNewTask.Create(nil);
   try
      Result := form.Execute(AnAPI, TheDetails, ADefaultProjectID);
   finally
      form.Free;
   end;
end;

{ TFormDialogTodoistNewTask }

// @todo enable OKButton when setup

procedure TFormDialogTodoistNewTask.editTitleChange(Sender: TObject);
begin
   TestTaskReady;
end;

procedure TFormDialogTodoistNewTask.cbProjectsSelect(Sender: TObject);
var
   p: TTodoistProject;
begin
   if (0 > cbProjects.ItemIndex) then begin
      Exit;
   end;
   p := TTodoistProject(cbProjects.ItemsEx[cbProjects.ItemIndex].Data);
   if Assigned(p) then begin
      cbSections.DisplayItems<TTodoistSection>(p.Sections);
   end;
end;

procedure TFormDialogTodoistNewTask.TestTaskReady;
begin
   panelButtons.OKButton.Enabled := Length(editTitle.Text) > 0;
end;

function TFormDialogTodoistNewTask.Execute(const AnAPI: TTodoistAPIv2; out TheDetails: TLayeredJSONObject; ADefaultProjectID: string): boolean;

   procedure FillProjects;
   var
      c: TComboExItem;
      ip: TTodoistProject;
      iDefault: integer;
      iIterator: integer;
   begin
      iDefault := -1;
      cbProjects.DisplayItems<TTodoistProject>(AnAPI.Projects);
      if Length(ADefaultProjectID) > 0 then begin
         for iIterator := 0 to Pred(cbProjects.ItemsEx.Count) do begin
            c := cbProjects.ItemsEx[iIterator];
            ip := TTodoistProject(c.Data);
            if Assigned(ip) then begin
               if SameText(ip.ProjectID, ADefaultProjectID) then begin
                  iDefault := iIterator;
               end;
            end;
         end;
         cbProjects.ItemIndex := iDefault;
      end;
   end;

var
   p: TTodoistProject;
   sec: TTodoistSection;
begin
   // fill project list
   TheDetails := nil;
   FillProjects;
   Result := (mrOk = ShowModal);
   if Result then begin
      TheDetails := TLayeredJSONObject.CreateInstance(True);
      TheDetails.AddString('content', editTitle.Text);
      TheDetails.AddString('description', memoDescription.Lines.Text);
      TheDetails.AddInt64('priority', Succ(cbPriority.ItemIndex));
      if (0 <= cbProjects.ItemIndex) then begin
         p := TTodoistProject(cbProjects.ItemsEx[cbProjects.ItemIndex].Data);
         if Assigned(p) then begin
            TheDetails.AddString('project_id', p.ProjectID);
         end;
      end;
      if (0 <= cbSections.ItemIndex) then begin
         sec := TTodoistSection(cbSections.ItemsEx[cbSections.ItemIndex].Data);
         if Assigned(sec) then begin
            TheDetails.AddString('section_id', sec.SectionID);
         end;
      end;
   end;
end;

end.
