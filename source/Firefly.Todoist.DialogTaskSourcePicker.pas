unit Firefly.Todoist.DialogTaskSourcePicker;

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ButtonPanel,
   Firefly.Todoist.ControlSourcePicker,
   Firefly.Todoist.V2;

type

   { TFormDialogTodoistTaskSourcePicker }

   TFormDialogTodoistTaskSourcePicker = class(TForm)
      panelButtons: TButtonPanel;
      pcSource: TFireflyTodoistSourcePicker;
      procedure FireflyTodoistSourcePicker1Select(AnTodoistObject: TTodoistContainerObject; AClose: boolean);
      procedure FormCreate(Sender: TObject);
      procedure FormShow(Sender: TObject);
   private
      FSelected: TTodoistContainerObject;
   public
      function Execute(const AnAPI: TTodoistAPIv2; out ATodoistObject: TTodoistContainerObject): boolean;
   end;

function ShowTodoistTaskSourcePicker(const AnAPI: TTodoistAPIv2; out ATodoistObject: TTodoistContainerObject): boolean;

implementation

function ShowTodoistTaskSourcePicker(const AnAPI: TTodoistAPIv2; out ATodoistObject: TTodoistContainerObject): boolean;
var
   form: TFormDialogTodoistTaskSourcePicker;
begin
   form := TFormDialogTodoistTaskSourcePicker.Create(nil);
   try
      Result := form.Execute(AnAPI, ATodoistObject);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormDialogTodoistTaskSourcePicker }

procedure TFormDialogTodoistTaskSourcePicker.FireflyTodoistSourcePicker1Select(AnTodoistObject: TTodoistContainerObject; AClose: boolean);
begin
   panelButtons.OKButton.Enabled := True;
   FSelected := AnTodoistObject;
   if AClose then begin
      panelButtons.OKButton.Click;
   end;
end;

procedure TFormDialogTodoistTaskSourcePicker.FormCreate(Sender: TObject);
begin
   FSelected := nil;
end;

procedure TFormDialogTodoistTaskSourcePicker.FormShow(Sender: TObject);
begin
   pcSource.OnSelect := @FireflyTodoistSourcePicker1Select;
end;

function TFormDialogTodoistTaskSourcePicker.Execute(const AnAPI: TTodoistAPIv2; out ATodoistObject: TTodoistContainerObject): boolean;
begin
   pcSource.API := AnAPI;
   Result := (ShowModal = mrOk);
   ATodoistObject := FSelected;
end;

end.
