unit Firefly.Todoist.FrameTask;

(*
Boxicons Regular Vol.3 icon pack
https://www.iconfinder.com/iconsets/boxicons-regular-vol-3
*)

{$mode ObjFPC}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   StdCtrls,
   ExtCtrls,
   Firefly.Todoist.V2;

type

   { TFrameTodoistTask }

   TFrameTodoistTask = class(TFrame)
      imgTask: TImage;
      labelLabels: TLabel;
      labelProject: TLabel;
      labelTaskName: TLabel;
      labelDescription: TLabel;
      Panel1: TPanel;
      Panel2: TPanel;
      Shape1: TShape;
      procedure labelProjectClick(Sender: TObject);
      procedure labelTaskNameClick(Sender: TObject);
   private
      FTask: TTodoistTask;
      procedure SetTask(AValue: TTodoistTask);
   public
      property Task: TTodoistTask read FTask write SetTask;
   end;

implementation

{$R *.lfm}

{ TFrameTodoistTask }

procedure TFrameTodoistTask.labelProjectClick(Sender: TObject);
begin

end;

procedure TFrameTodoistTask.labelTaskNameClick(Sender: TObject);
begin

end;

procedure TFrameTodoistTask.SetTask(AValue: TTodoistTask);
begin
   FTask := AValue;
   labelTaskName.Caption := FTask.Content;
   labelDescription.Caption := FTask.Description;
   labelDescription.Visible := Length(FTask.Description) > 0;
   labelLabels.Caption := FTask.Labels;
end;

end.
