{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Form to pick a Todoist project or section.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-03  pk  ---  First implementation.
// *****************************************************************************
   )
}

unit Firefly.Todoist.ProjectPicker;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   ButtonPanel,
   ExtCtrls,
   Firefly.Todoist.V2,
   Firefly.Todoist.UI;

type

   { TFormTodoistProjectPicker }

   TFormTodoistProjectPicker = class(TForm)
      ButtonPanel1: TButtonPanel;
      ilTodoistTypes: TImageList;
      tv: TTreeView;
      procedure tvDblClick({%H-}Sender: TObject);
   private
      FProjects: TTodoistProjects;
      procedure FillTree(AIncludeSections: boolean);
      function FindProjectByID(TheID: string): TTreeNode;
   public
      function PickProject(AProjects: TTodoistProjects): TTodoistProject;
      function PickProjectOrSection(AProjects: TTodoistProjects): TTodoistContainerObject;
   end;

function PickProject(AProjects: TTodoistProjects): TTodoistProject;
function PickProjectOrSection(AProjects: TTodoistProjects): TTodoistContainerObject;

implementation

function PickProject(AProjects: TTodoistProjects): TTodoistProject;
var
   form: TFormTodoistProjectPicker;
begin
   form := TFormTodoistProjectPicker.Create(nil);
   try
      Result := form.PickProject(AProjects);
   finally
      form.Free;
   end;
end;

function PickProjectOrSection(AProjects: TTodoistProjects): TTodoistContainerObject;
var
   form: TFormTodoistProjectPicker;
begin
   form := TFormTodoistProjectPicker.Create(nil);
   try
      Result := form.PickProjectOrSection(AProjects);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormTodoistProjectPicker }

procedure TFormTodoistProjectPicker.tvDblClick(Sender: TObject);
begin
   Self.ModalResult := mrOk;
end;

procedure TFormTodoistProjectPicker.FillTree(AIncludeSections: boolean);
var
   p: TTodoistProject;
   s: TTodoistSection;
   nParent: TTreeNode;
   nProject: TTreeNode;
   nSection: TTreeNode;
begin
   for p in FProjects do begin
      nParent := FindProjectByID(p.ParentID);
      nProject := tv.Items.AddChildObject(nParent, p.ProjectName, p);
      nProject.ImageIndex := 0;
      nProject.SelectedIndex := 0;
      if AIncludeSections then begin
         for s in p.Sections do begin
            nSection := tv.Items.AddChildObject(nProject, s.SectionName, s);
            nSection.ImageIndex := 1;
            nSection.SelectedIndex := 1;
         end;
      end;
   end;
   tv.FullExpand;
end;

function TFormTodoistProjectPicker.FindProjectByID(TheID: string): TTreeNode;
var
   n: TTreeNode;
   p: TTodoistProject;
begin
   for n in tv.Items do begin
      p := TTodoistProject(n.Data);
      if (TheID = p.ProjectID) then begin
         Result := n;
         Exit;
      end;
   end;
   Result := nil;
end;

function TFormTodoistProjectPicker.PickProject(AProjects: TTodoistProjects): TTodoistProject;
begin
   FProjects := AProjects;
   ProjectsToTreeView(FProjects, tv, false);
   Result := nil;
   if (ShowModal = mrOk) and (Assigned(tv.Selected)) then begin
      Result := TTodoistProject(tv.Selected.Data);
   end;
end;

function TFormTodoistProjectPicker.PickProjectOrSection(AProjects: TTodoistProjects): TTodoistContainerObject;
begin
   FProjects := AProjects;
   ProjectsToTreeView(FProjects, tv, true);
   Result := nil;
   if (ShowModal = mrOk) and (Assigned(tv.Selected)) then begin
      Result := TTodoistContainerObject(tv.Selected.Data);
   end;
end;

end.
