unit Firefly.Todoist.UI;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   ComboEx,
   ComCtrls,
   Firefly.Todoist.V2;

procedure ProjectsToComboBoEx(AProjects: TTodoistProjects; AComboBox: TComboBoxEx; AAddSections: boolean = False);
procedure SectionsToComboBoEx(ASections: TTodoistSections; AComboBox: TComboBoxEx);
function FindContainerByIDInComboBox(TheID: string; AComboBox: TComboBoxEx): TComboExItem;
function GetSelectedContainerInComboBox(AComboBox: TComboBoxEx): TComboExItem;
procedure SetSelectedContainerInComboBox(AComboBox: TComboBoxEx; AContainer: TTodoistContainerObject); overload;
procedure SetSelectedContainerInComboBox(AComboBox: TComboBoxEx; AContainerID: string); overload;

procedure ProjectsToTreeView(AProjects: TTodoistProjects; ATreeView: TTreeView; AAddSections: boolean = False);
function FindContainerByIDInTreeView(TheID: string; ATreeView: TTreeView): TTreeNode;

implementation

procedure ProjectsToComboBoEx(AProjects: TTodoistProjects; AComboBox: TComboBoxEx; AAddSections: boolean);
var
   p: TTodoistProject;
   s: TTodoistSection;
   nParent: TComboExItem;
   nProject: TComboExItem;
   sections: TTodoistSections;
begin
   for p in AProjects do begin
      nParent := FindContainerByIDInComboBox(p.ParentID, AComboBox);
      nProject := ACombobox.ItemsEx.AddItem(p.ProjectName, 0, 0, 0, 0, p);
      if Assigned(nParent) then begin
         nProject.Indent := nParent.Indent + 10;
      end;
      if AAddSections then begin
         sections := p.Sections;
         for s in sections do begin
            ACombobox.ItemsEx.AddItem(s.SectionName, 1, 1, 1, nProject.Indent + 10, s);
         end;
      end;
   end;
end;

procedure SectionsToComboBoEx(ASections: TTodoistSections; AComboBox: TComboBoxEx);
var
   s: TTodoistSection;
begin
   for s in ASections do begin
      ACombobox.ItemsEx.AddItem(s.SectionName, 1, 1, 1, -1, s);
   end;
end;

function FindContainerByIDInComboBox(TheID: string; AComboBox: TComboBoxEx
  ): TComboExItem;
var
   n: TComboExItem;
   p: TTodoistContainerObject;
   i: integer;
begin
   //for n in AComboBox.ItemsEx do begin
   for i := 0 to Pred(AComboBox.ItemsEx.Count) do begin
      n := AComboBox.ItemsEx[i];
      p := TTodoistContainerObject(n.Data);
      if (TheID = p.ID) then begin
         Result := n;
         Exit;
      end;
   end;
   Result := nil;
end;

function GetSelectedContainerInComboBox(AComboBox: TComboBoxEx): TComboExItem;
begin
   Result := nil;
   if AComboBox.ItemIndex < 0 then begin
      Exit;
   end;
   Result := AComboBox.ItemsEx[AComboBox.ItemIndex];
end;

procedure SetSelectedContainerInComboBox(AComboBox: TComboBoxEx; AContainer: TTodoistContainerObject);
var
   n: TComboExItem;
   p: TTodoistContainerObject;
   i: integer;
begin
   for i := 0 to Pred(AComboBox.ItemsEx.Count) do begin
      n := AComboBox.ItemsEx[i];
      p := TTodoistContainerObject(n.Data);
      if (AContainer = p) then begin
         AComboBox.ItemIndex := i;
         Exit;
      end;
   end;
   AComboBox.ItemIndex := -1;
end;

procedure SetSelectedContainerInComboBox(AComboBox: TComboBoxEx; AContainerID: string);
var
   n: TComboExItem;
   p: TTodoistContainerObject;
   i: integer;
begin
   for i := 0 to Pred(AComboBox.ItemsEx.Count) do begin
      n := AComboBox.ItemsEx[i];
      p := TTodoistContainerObject(n.Data);
      if (Assigned(p) and (AContainerID = p.ID)) or ((not Assigned(p)) and ('' = AContainerID)) then begin
         AComboBox.ItemIndex := i;
         Exit;
      end;
   end;
   AComboBox.ItemIndex := -1;
end;

procedure ProjectsToTreeView(AProjects: TTodoistProjects; ATreeView: TTreeView; AAddSections: boolean);
var
   p: TTodoistProject;
   s: TTodoistSection;
   nParent: TTreeNode;
   nProject: TTreeNode;
   nSection: TTreeNode;
begin
   for p in AProjects do begin
      nParent := FindContainerByIDInTreeView(p.ParentID, ATreeView);
      nProject := ATreeView.Items.AddChildObject(nParent, p.ProjectName, p);
      nProject.ImageIndex := 0;
      nProject.SelectedIndex := 0;
      if AAddSections then begin
         for s in p.Sections do begin
            nSection := ATreeView.Items.AddChildObject(nProject, s.SectionName, s);
            nSection.ImageIndex := 1;
            nSection.SelectedIndex := 1;
         end;
      end;
   end;
   ATreeView.FullExpand;
end;

function FindContainerByIDInTreeView(TheID: string; ATreeView: TTreeView
  ): TTreeNode;
var
   n: TTreeNode;
   p: TTodoistContainerObject;
begin
   for n in ATreeView.Items do begin
      p := TTodoistContainerObject(n.Data);
      if (TheID = p.ID) then begin
         Result := n;
         Exit;
      end;
   end;
   Result := nil;
end;

end.
