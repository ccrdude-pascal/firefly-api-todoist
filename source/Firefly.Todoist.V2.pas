{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Access to the Todoist API.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-03  pk  ---  Added sections.
// 2023-08-02  pk  ---  First draft listing projects.
// *****************************************************************************
   )
}

unit Firefly.Todoist.V2;

{$IFDEF FPC}
{$mode Delphi}{$H+}
{$ENDIF FPC}

interface

 // @url(https://todoist.com/showTask?id=7105074301 Create separate package)
 // @url(https://todoist.com/showTask?id=7105074444 Publish package as open source)
 // @url(https://todoist.com/showTask?id=7105223079 Create tasks)
 // @url(https://todoist.com/showTask?id=7105222929 Read comments)

uses
   Classes,
   SysUtils,
   Generics.Collections,
   Layers.Transport.Base,
   Layers.JSON.Base,
   httpprotocol,
   OVM.ListView.Attributes,
   OVM.ComboBox.Attributes,
   OVM.TreeView.Attributes;

type
   TTodoistAPIv2 = class;
   TTodoistTask = class;
   TTodoistTasks = class;
   TTodoistSection = class;
   TTodoistSections = class;
   TTodoistProject = class;
   TTodoistProjects = class;
   TTodoistComments = class;

   { TTodoistDataObject }

   TTodoistDataObject = class abstract
   protected
      FOwner: TObject;
      FAPI: TTodoistAPIv2;
   public
      constructor Create(AnAPI: TTodoistAPIv2); virtual;
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); virtual; abstract;
      function DisplayText: string; virtual; abstract;
   end;

   {
     TTodoistDataObjects is the parent class all data containers should
     inherit from; it implements reading from a JSON array into the items.
   }
   TTodoistDataObjects<T: TTodoistDataObject> = class(TObjectList<T>)
   protected
      FOwner: TObject;
      FAPI: TTodoistAPIv2;
      procedure ForEachObject({%H-}TheElementName: string; TheElement: TLayeredJSONObject; {%H-}AnUserData: pointer; var Continue: boolean); virtual;
   public
      constructor Create(AnAPI: TTodoistAPIv2); overload;
      procedure LoadFromJSONArray(AnArray: TLayeredJSONArray);
      function FindUsingID(ID: string): T;
   end;

   { TTodoistContainerObject }

   TTodoistContainerObject = class(TTodoistDataObject)
   protected
      FTasks: TTodoistTasks;
      function GetID: string; virtual; abstract;
      function GetTasks: TTodoistTasks; virtual; abstract;
   public
      constructor Create(AnAPI: TTodoistAPIv2); override;
      destructor Destroy; override;
      property Tasks: TTodoistTasks read GetTasks;
   published
      [AComboBoxUniqueIDField(), ATreeViewUniqueIDField(), AListViewSkipField()]
      property ID: string read GetID;
   end;

   { TTodoistTask }

   TTodoistTask = class(TTodoistDataObject)
   private
      FAssigneeID: string;
      FAssignerID: string;
      FCommentCount: integer;
      FComments: TTodoistComments;
      FContent: string;
      FCreatorID: string;
      FDescription: string;
      FIsCompleted: boolean;
      FLabels: TStringList;
      FOrder: integer;
      FParentID: string;
      FPriority: integer;
      FProjectID: string;
      FSectionID: string;
      FTaskID: string;
      FURL: string;
      function GetComments: TTodoistComments;
      function GetLabels: string;
   public
      constructor Create(AnAPI: TTodoistAPIv2); override;
      destructor Destroy; override;
      procedure RefreshComments;
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property Comments: TTodoistComments read GetComments;
   published
      [AListViewColumnDetails('ID')]
      property TaskID: string read FTaskID;
      [AListViewColumnDetails('Title')]
      property Content: string read FContent;
      [AListViewColumnDetails('Description'), AListViewTileColumnField()]
      property Description: string read FDescription;
      [AListViewColumnDetails('Labels'), AListViewTileColumnField()]
      property Labels: string read GetLabels;
      [AListViewColumnDetails('Comments', taRightJustify)]
      property CommentCount: integer read FCommentCount;
      [AListViewColumnDetails('Prio', taRightJustify)]
      property Priority: integer read FPriority;
      property Order: integer read FOrder;
      property IsCompleted: boolean read FIsCompleted;
      property URL: string read FURL;
      property CreatorID: string read FCreatorID;
      property AssigneeID: string read FAssigneeID;
      property AssignerID: string read FAssignerID;
      property ProjectID: string read FProjectID;
      property SectionID: string read FSectionID;
      property ParentID: string read FParentID;
   end;

   { TTodoistTasks }

   TTodoistTasks = class(TTodoistDataObjects<TTodoistTask>);

   { TTodoistSection }

   TTodoistSection = class(TTodoistContainerObject)
   private
      FOrder: integer;
      FProjectID: string;
      FSectionID: string;
      FSectionName: string;
      function GetProject: TTodoistProject;
   protected
      function GetID: string; override;
      function GetTasks: TTodoistTasks; override;
   public
      constructor Create(AnAPI: TTodoistAPIv2); override;
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property SectionID: string read FSectionID;
      property ProjectID: string read FProjectID;
      property Project: TTodoistProject read GetProject;
   published
      [AComboBoxDisplayText()]
      property SectionName: string read FSectionName;
      property Order: integer read FOrder;
   end;

   { TTodoistSections }

   TTodoistSections = class(TTodoistDataObjects<TTodoistSection>);

   { TTodoistProject }

   TTodoistProject = class(TTodoistContainerObject)
   private
      FColorName: string;
      FCommentCount: integer;
      FComments: TTodoistComments;
      FIsFavorite: boolean;
      FIsInboxProject: boolean;
      FIsShared: boolean;
      FIsTeamInbox: boolean;
      FOrder: integer;
      FParentID: string;
      FProjectID: string;
      FProjectName: string;
      FSections: TTodoistSections;
      FURL: string;
      FViewStyleText: string;
      function GetComments: TTodoistComments;
      function GetSections: TTodoistSections;
   protected
      function GetID: string; override;
      function GetTasks: TTodoistTasks; override;
   public
      constructor Create(AnAPI: TTodoistAPIv2); override;
      destructor Destroy; override;
      procedure RefreshComments;
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property ProjectID: string read FProjectID;
      property ColorName: string read FColorName;
      property IsInboxProject: boolean read FIsInboxProject;
      property IsTeamInbox: boolean read FIsTeamInbox;
      property URL: string read FURL;
      property Sections: TTodoistSections read GetSections;
      property Comments: TTodoistComments read GetComments;
   published
      [AComboBoxDisplayText(), ATreeViewDisplayText()]
      property ProjectName: string read FProjectName;
      property Order: integer read FOrder;
      property IsShared: boolean read FIsShared;
      property IsFavorite: boolean read FIsFavorite;
      property CommentCount: integer read FCommentCount;
      [AComboBoxParentIDField(), ATreeViewParentIDField()]
      property ParentID: string read FParentID;
   end;

   { TTodoistProjects }

   TTodoistProjects = class(TTodoistDataObjects<TTodoistProject>);

   { TTodoistComment }

   // @todo : attachments
   TTodoistComment = class(TTodoistDataObject)
   private
      FCommentID: string;
      FContent: string;
      FPostedAt: string;
   public
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      property CommentID: string read FCommentID;
      property PostedAt: string read FPostedAt;
   published
      property Content: string read FContent;
   end;

   TTodoistComments = class(TTodoistDataObjects<TTodoistComment>);

   { TTodoistLabel }

   TTodoistLabel = class(TTodoistContainerObject)
   private
      FColorName: string;
      FIsFavorite: boolean;
      FLabelID: string;
      FLabelName: string;
      FOrder: integer;
   protected
      function GetID: string; override;
      function GetTasks: TTodoistTasks; override;
   public
      constructor Create(AnAPI: TTodoistAPIv2); override;
      destructor Destroy; override;
      procedure LoadFromJSONObject(AnObject: TLayeredJSONObject); override;
      function DisplayText: string; override;
      property LabelID: string read FLabelID;
   published
      [AComboBoxDisplayText(), ATreeViewDisplayText()]
      property LabelName: string read FLabelName;
      property ColorName: string read FColorName;
      property Order: integer read FOrder;
      property IsFavorite: boolean read FIsFavorite;
   end;

   TTodoistLabels = class(TTodoistDataObjects<TTodoistLabel>);

   { TTodoistAPIv2 }

   TTodoistAPIv2 = class
   private
      FProjects: TTodoistProjects;
      FLabels: TTodoistLabels;
      FProxyHost: string;
      FProxyPort: word;
      FToken: string;
      FUserAgent: string;
      function CreateTransport: TLayeredTransport;
      function GetLabels: TTodoistLabels;
      function GetProjects: TTodoistProjects;
   protected
      function ExecuteGET(AnURL: string; out AnArray: TLayeredJSONArray): boolean; overload;
      function ExecuteGET(AnURL: string; out AnObject: TLayeredJSONObject): boolean; overload;
      function ExecutePOST(AnURL: string): boolean; overload;
      function ExecutePOST(AnURL: string; AnInput, AnOutput: TLayeredJSONObject): boolean; overload;
      function GetTaskJSON(AContent: string; APriority: integer): TLayeredJSONObject; virtual;
   public
      constructor Create; virtual;
      destructor Destroy; override;
      function HasToken: boolean;
      function RefreshProjects: boolean;
      function RefreshLabels: boolean;
      function RefreshSections(AProject: TTodoistProject): boolean;
      function RefreshTasks(AProject: TTodoistProject): boolean; overload;
      function RefreshTasks(ASection: TTodoistSection): boolean; overload;
      function RefreshTasks(ALabel: TTodoistLabel): boolean; overload;
      function RefreshTasks(AContainer: TTodoistContainerObject): boolean; overload;
      function GetTask(AnID: string; out ATask: TTodoistTask): boolean;
      // @url(https://todoist.com/showTask?id=7105074444 Implement task query by IDs for this)
      function RefreshTasks(IDs: array of uint64): TTodoistTasks; overload;
      function CloseTask(ATask: TTodoistTask): boolean; overload;
      function CloseTask(ATaskID: string): boolean; overload;
      function RefreshTaskComments(ATask: TTodoistTask): boolean;
      function RefreshProjectComments(AProject: TTodoistProject): boolean;
      function AddTaskComment(ATask: TTodoistTask; AComment: string): boolean; overload;
      function AddTaskComment(ATaskID: string; AComment: string): boolean; overload;
      function AddTask(out ATaskID: string; ATaskJSON: TLayeredJSONObject): boolean;
      function AddTaskToProject(out ATaskID: string; AProjectID: string; AContent: string; APriority: integer = 4): boolean; overload;
      function AddTaskToProject(out ATask: TTodoistTask; AProjectID: string; AContent: string; APriority: integer = 4): boolean; overload;
      function AddTaskWithLabel(out ATaskID: string; ALabel: string; AContent: string; APriority: integer = 4): boolean;
      property Token: string read FToken write FToken;
      property Projects: TTodoistProjects read GetProjects;
      property Labels: TTodoistLabels read GetLabels;
      property ProxyHost: string read FProxyHost write FProxyHost;
      property ProxyPort: word read FProxyPort write FProxyPort;
      property UserAgent: string read FUserAgent write FUserAgent;
   end;

implementation

{ TTodoistSection }

function TTodoistSection.GetProject: TTodoistProject;
begin
   Result := TTodoistProject(TTodoistSections(Self.FOwner).FOwner);
end;

function TTodoistSection.GetTasks: TTodoistTasks;
begin
   TTodoistAPIv2(TTodoistProjects(TTodoistProject(TTodoistSections(Self.FOwner).FOwner).FOwner).FOwner).RefreshTasks(Self);
   Result := FTasks;
end;

function TTodoistSection.GetID: string;
begin
   Result := Self.SectionID;
end;

constructor TTodoistSection.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(AnAPI);
   FTasks.FOwner := Self.FOwner;
end;

procedure TTodoistSection.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindString('id', Self.FSectionID);
   AnObject.FindString('project_id', Self.FProjectID);
   AnObject.FindInteger('order', Self.FOrder);
   AnObject.FindString('name', Self.FSectionName);
end;

function TTodoistSection.DisplayText: string;
begin
   Result := Self.Project.DisplayText + ': ' + Self.FSectionName;
end;

{ TTodoistProject }

function TTodoistProject.GetSections: TTodoistSections;
begin
   TTodoistAPIv2(TTodoistProjects(Self.FOwner).FOwner).RefreshSections(Self);
   Result := FSections;
end;

function TTodoistProject.GetComments: TTodoistComments;
begin
   if (FComments.Count = 0) and (FCommentCount > 0) then begin
      RefreshComments;
   end;
   Result := FComments;
end;

function TTodoistProject.GetID: string;
begin
   Result := Self.ProjectID;
end;

function TTodoistProject.GetTasks: TTodoistTasks;
begin
   TTodoistAPIv2(TTodoistProjects(Self.FOwner).FOwner).RefreshTasks(Self);
   Result := FTasks;
end;

constructor TTodoistProject.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(AnAPI);
   FSections := TTodoistSections.Create(AnAPI);
   FSections.FOwner := Self;
   FTasks.FOwner := Self;
end;

destructor TTodoistProject.Destroy;
begin
   FSections.Free;
   inherited Destroy;
end;

procedure TTodoistProject.RefreshComments;
begin
   FAPI.RefreshProjectComments(Self);
end;

procedure TTodoistProject.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindString('id', Self.FProjectID);
   AnObject.FindString('parent_id', Self.FParentID);
   AnObject.FindInteger('order', Self.FOrder);
   AnObject.FindString('color', Self.FColorName);
   AnObject.FindString('name', Self.FProjectName);
   AnObject.FindInteger('comment_count', Self.FCommentCount);
   AnObject.FindBoolean('is_shared', Self.FIsShared);
   AnObject.FindBoolean('is_favorite', Self.FIsFavorite);
   AnObject.FindBoolean('is_inbox_project', Self.FIsInboxProject);
   AnObject.FindBoolean('is_team_inbox', Self.FIsTeamInbox);
   AnObject.FindString('url', Self.FURL);
   AnObject.FindString('view_style', Self.FViewStyleText);
   {
   "id": "2193800809",
   "parent_id": "2152281538",
   "order": 8,
   "color": "sky_blue",
   "name": "Spybot 3",
   "comment_count": 0,
   "is_shared": true,
   "is_favorite": false,
   "is_inbox_project": false,
   "is_team_inbox": false,
   "url": "https://todoist.com/showProject?id=2193800809",
   "view_style": "list"
   }
end;

function TTodoistProject.DisplayText: string;
begin
   Result := Self.ProjectName;
end;

{ TTodoistComment }

procedure TTodoistComment.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindString('id', Self.FCommentID);
   AnObject.FindString('content', Self.FContent);
   AnObject.FindString('posted_at', Self.FPostedAt);
end;

{ TTodoistLabel }

function TTodoistLabel.GetID: string;
begin
   Result := Self.FLabelID;
end;

function TTodoistLabel.GetTasks: TTodoistTasks;
begin
   TTodoistAPIv2(TTodoistTasks(Self.FOwner).FOwner).RefreshTasks(Self);
   Result := FTasks;
end;

constructor TTodoistLabel.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(AnAPI);
   FTasks.FOwner := Self;
end;

destructor TTodoistLabel.Destroy;
begin
   inherited Destroy;
end;

procedure TTodoistLabel.LoadFromJSONObject(AnObject: TLayeredJSONObject);
begin
   AnObject.FindString('id', Self.FLabelID);
   AnObject.FindInteger('order', Self.FOrder);
   AnObject.FindString('color', Self.FColorName);
   AnObject.FindString('name', Self.FLabelName);
   AnObject.FindBoolean('is_favorite', Self.FIsFavorite);
end;

function TTodoistLabel.DisplayText: string;
begin
   Result := Self.LabelName;
end;

{ TTodoistTasks }

function TTodoistTask.GetLabels: string;
begin
   FLabels.Delimiter := ',';
   FLabels.StrictDelimiter := True;
   Result := FLabels.DelimitedText;
end;

function TTodoistTask.GetComments: TTodoistComments;
begin
   if (FComments.Count = 0) and (FCommentCount > 0) then begin
      RefreshComments;
   end;
   Result := FComments;
end;

constructor TTodoistTask.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(AnAPI);
   FLabels := TStringList.Create;
   FComments := TTodoistComments.Create(AnAPI);
end;

destructor TTodoistTask.Destroy;
begin
   FComments.Free;
   FLabels.Free;
   inherited Destroy;
end;

procedure TTodoistTask.RefreshComments;
var
   api: TTodoistAPIv2;
begin
   FAPI.RefreshTaskComments(Self);
end;

procedure TTodoistTask.LoadFromJSONObject(AnObject: TLayeredJSONObject);
var
   a: TLayeredJSONArray;
begin
   AnObject.FindString('creator_id', Self.FCreatorID);
   AnObject.FindString('asignee_id', Self.FAssigneeID);
   AnObject.FindString('assigner_id', Self.FAssignerID);
   AnObject.FindInteger('comment_count', Self.FCommentCount);
   // TODO : Task support for comments
   AnObject.FindString('content', Self.FContent);
   AnObject.FindString('description', Self.FDescription);
   AnObject.FindBoolean('is_completed', Self.FIsCompleted);
   (*
   "due": {
            "date": "2016-09-01",
            "is_recurring": false,
            "datetime": "2016-09-01T12:00:00.000000Z",
            "string": "tomorrow at 12",
            "timezone": "Europe/Moscow"
        },
   "duration": null,
   *)
   // TODO : Task support for due
   // TODO : Task support for duration
   AnObject.FindString('id', Self.FTaskID);
   FLabels.Clear;
   if AnObject.FindArray('labels', a) then begin
      a.GetStrings(FLabels);
   end;
   AnObject.FindInteger('order', Self.FOrder);
   AnObject.FindInteger('priority', Self.FPriority);
   AnObject.FindString('project_id', Self.FProjectID);
   AnObject.FindString('section_id', Self.FSectionID);
   AnObject.FindString('parent_id', Self.FParentID);
   AnObject.FindString('url', Self.FURL);
end;

function TTodoistTask.DisplayText: string;
begin
   Result := Self.FContent;
end;

{ TTodoistDataObjects }

procedure TTodoistDataObjects<T>.ForEachObject(TheElementName: string; TheElement: TLayeredJSONObject; AnUserData: pointer; var Continue: boolean);
var
   Data: T;
begin
   Data := T.Create(FAPI);
   if (Data is TTodoistDataObject) then begin
      TTodoistDataObject(Data).LoadFromJSONObject(TheElement);
      TTodoistDataObject(Data).FOwner := Self;
   end;
   Self.Add(Data);
   Continue := True;
end;

constructor TTodoistDataObjects<T>.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(True);
   FAPI := AnAPI;
end;

procedure TTodoistDataObjects<T>.LoadFromJSONArray(AnArray: TLayeredJSONArray);
begin
   Self.Clear;
   AnArray.ForEachObject(ForEachObject, nil);
end;

function TTodoistDataObjects<T>.FindUsingID(ID: string): T;
var
   o: T;
   od: TTodoistDataObject;
begin
   Result := nil;
   for o in Self do begin
      if o is TTodoistDataObject then begin
         od := TTodoistDataObject(o);
         if od is TTodoistContainerObject then begin
            if SameText(TTodoistContainerObject(od).ID, ID) then begin
               Result := o;
               Exit;
            end;
         end;
      end;
   end;
end;

{ TTodoistContainerObject }

constructor TTodoistContainerObject.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create(AnAPI);
   FTasks := TTodoistTasks.Create(AnAPI);
   FTasks.FOwner := Self;
end;

destructor TTodoistContainerObject.Destroy;
begin
   FTasks.Free;
   inherited Destroy;
end;


{ TTodoistAPIv2 }

function TTodoistAPIv2.CreateTransport: TLayeredTransport;
begin
   Result := TLayeredTransport.CreateInstance;
   if Length(Self.FProxyHost) > 0 then begin
      Result.Proxy.Host := Self.FProxyHost;
      Result.Proxy.Port := IntToStr(Self.FProxyPort);
   end;
   Result.UserAgent := Self.UserAgent;
   Result.ContentType := 'application/json';
   Result.Headers.Add('Authorization=Bearer ' + Self.Token);
end;

function TTodoistAPIv2.ExecuteGET(AnURL: string; out AnArray: TLayeredJSONArray): boolean;
var
   transport: TLayeredTransport;
begin
   AnArray := TLayeredJSONArray.CreateInstance(False);
   transport := CreateTransport;
   try
      Result := transport.GET(AnURL, AnArray);
      if not Result then begin
         FreeAndNil(AnArray);
      end;
   finally
      transport.Free;
   end;
end;

function TTodoistAPIv2.ExecuteGET(AnURL: string; out AnObject: TLayeredJSONObject): boolean;
var
   transport: TLayeredTransport;
begin
   AnObject := TLayeredJSONObject.CreateInstance(False);
   transport := CreateTransport;
   try
      Result := transport.GET(AnURL, AnObject);
      if not Result then begin
         FreeAndNil(AnObject);
      end;
   finally
      transport.Free;
   end;
end;

function TTodoistAPIv2.ExecutePOST(AnURL: string): boolean;
var
   transport: TLayeredTransport;
begin
   transport := CreateTransport;
   try
      Result := transport.POST(AnURL);
   finally
      transport.Free;
   end;
end;

function TTodoistAPIv2.ExecutePOST(AnURL: string; AnInput, AnOutput: TLayeredJSONObject): boolean;
var
   transport: TLayeredTransport;
begin
   transport := CreateTransport;
   try
      Result := transport.POST(AnURL, AnInput, AnOutput);
   finally
      transport.Free;
   end;
end;

function TTodoistAPIv2.GetLabels: TTodoistLabels;
begin
   if (0 = FLabels.Count) then begin
      Self.RefreshLabels;
   end;
   Result := FLabels;
end;

function TTodoistAPIv2.GetProjects: TTodoistProjects;
begin
   if (0 = FProjects.Count) then begin
      Self.RefreshProjects;
   end;
   Result := FProjects;
end;

function TTodoistAPIv2.RefreshSections(AProject: TTodoistProject): boolean;
var
   json: TLayeredJSONArray;
   s: string;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/sections?project_id=' + AProject.ProjectID, json);
   if Result then begin
      try
         AProject.FSections.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshTasks(AProject: TTodoistProject): boolean;
var
   json: TLayeredJSONArray;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/tasks?project_id=' + AProject.ProjectID, json);
   if Result then begin
      try
         AProject.FTasks.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshTasks(ASection: TTodoistSection): boolean;
var
   json: TLayeredJSONArray;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/tasks?section_id=' + ASection.SectionID, json);
   if Result then begin
      try
         ASection.FTasks.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshTasks(ALabel: TTodoistLabel): boolean;
var
   json: TLayeredJSONArray;
   sName: string;
begin
   sName := HTTPEncode(ALabel.LabelName);
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/tasks?label=' + sName, json);
   if Result then begin
      try
         ALabel.FTasks.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshTasks(AContainer: TTodoistContainerObject): boolean;
begin
   if AContainer is TTodoistProject then begin
      Result := RefreshTasks(AContainer as TTodoistProject);
   end else if AContainer is TTodoistSection then begin
      Result := RefreshTasks(AContainer as TTodoistSection);
   end else begin
      Result := False;
   end;
end;

function TTodoistAPIv2.GetTask(AnID: string; out ATask: TTodoistTask): boolean;
var
   json: TLayeredJSONObject;
begin
   ATask := TTodoistTask.Create(Self);
   ATask.FOwner := nil;
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/tasks/' + AnID, json);
   if Result then begin
      try
         ATask.LoadFromJSONObject(json);
      finally
         json.Free;
      end;
   end else begin
      FreeAndNil(ATask);
   end;
end;

function TTodoistAPIv2.RefreshTasks(IDs: array of uint64): TTodoistTasks;
var
   json: TLayeredJSONArray;
   sIDs: string;
   i: integer;
begin
   Result := TTodoistTasks.Create(Self);
   Result.FOwner := nil;
   sIDs := '';
   for i := Low(IDs) to High(IDs) do begin
      sIDs += IntToStr(IDs[i]);
      if (i <> High(IDs)) then begin
         sIDs += ',';
      end;
   end;
   if Self.ExecuteGET('https://api.todoist.com/rest/v2/tasks?ids=' + sIDs, json) then begin
      try
         Result.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end else begin
      FreeAndNil(Result);
   end;
end;

function TTodoistAPIv2.CloseTask(ATask: TTodoistTask): boolean;
begin
   Result := CloseTask(ATask.TaskID);
end;

function TTodoistAPIv2.CloseTask(ATaskID: string): boolean;
begin
   Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/tasks/' + ATaskID + '/close');
end;

function TTodoistAPIv2.RefreshTaskComments(ATask: TTodoistTask): boolean;
var
   json: TLayeredJSONArray;
   s: string;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/comments?task_id=' + ATask.TaskID, json);
   if Result then begin
      try
         ATask.FComments.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshProjectComments(AProject: TTodoistProject): boolean;
var
   json: TLayeredJSONArray;
   s: string;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/comments?task_id=' + AProject.ProjectID, json);
   if Result then begin
      try
         AProject.FComments.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.AddTaskComment(ATask: TTodoistTask; AComment: string): boolean;
begin
   Result := AddTaskComment(ATask.TaskID, AComment);
end;

function TTodoistAPIv2.AddTaskComment(ATaskID: string; AComment: string): boolean;
var
   jsonInput: TLayeredJSONObject;
   jsonOutput: TLayeredJSONObject;
begin
   jsonInput := TLayeredJSONObject.CreateInstance(True);
   try
      jsonInput.AddString('task_id', ATaskID);
      jsonInput.AddString('content', AComment);
      jsonOutput := TLayeredJSONObject.CreateInstance(True);
      try
         Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/comments', jsonInput, jsonOutput);
      finally
         jsonOutput.Free;
      end;
   finally
      jsonInput.Free;
   end;
end;

function TTodoistAPIv2.AddTask(out ATaskID: string; ATaskJSON: TLayeredJSONObject): boolean;
var
   jsonOutput: TLayeredJSONObject;
   t: TTodoistTask;
begin
   jsonOutput := TLayeredJSONObject.CreateInstance(True);
   try
      Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/tasks', ATaskJSON, jsonOutput);
      if Result then begin
         t := TTodoistTask.Create(Self);
         try
            t.LoadFromJSONObject(jsonOutput);
            ATaskID := t.TaskID;
         finally
            t.Free;
         end;
      end;
   finally
      jsonOutput.Free;
   end;
end;

function TTodoistAPIv2.GetTaskJSON(AContent: string; APriority: integer): TLayeredJSONObject;
begin
   Result := TLayeredJSONObject.CreateInstance(True);
   Result.AddString('content', AContent);
   Result.AddInt64('priority', APriority);
end;

function TTodoistAPIv2.AddTaskToProject(out ATaskID: string; AProjectID: string; AContent: string; APriority: integer): boolean;
var
   jsonInput: TLayeredJSONObject;
   jsonOutput: TLayeredJSONObject;
   t: TTodoistTask;
begin
   jsonInput := GetTaskJSON(AContent, APriority);
   try
      jsonInput.AddString('project_id', AProjectID);
      jsonOutput := TLayeredJSONObject.CreateInstance(True);
      try
         Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/tasks', jsonInput, jsonOutput);
         if Result then begin
            t := TTodoistTask.Create(Self);
            try
               t.LoadFromJSONObject(jsonOutput);
               ATaskID := t.TaskID;
            finally
               t.Free;
            end;
         end;
      finally
         jsonOutput.Free;
      end;
   finally
      jsonInput.Free;
   end;
end;

function TTodoistAPIv2.AddTaskToProject(out ATask: TTodoistTask; AProjectID: string; AContent: string; APriority: integer): boolean;
var
   jsonInput: TLayeredJSONObject;
   jsonOutput: TLayeredJSONObject;
   t: TTodoistTask;
begin
   ATask := nil;
   jsonInput := GetTaskJSON(AContent, APriority);
   try
      jsonInput.AddString('project_id', AProjectID);
      jsonOutput := TLayeredJSONObject.CreateInstance(True);
      try
         Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/tasks', jsonInput, jsonOutput);
         if Result then begin
            ATask := TTodoistTask.Create(Self);
            try
               ATask.LoadFromJSONObject(jsonOutput);
            except
               Result := False;
               FreeAndNil(ATask);
            end;
         end;
      finally
         jsonOutput.Free;
      end;
   finally
      jsonInput.Free;
   end;
end;

function TTodoistAPIv2.AddTaskWithLabel(out ATaskID: string; ALabel: string; AContent: string; APriority: integer): boolean;
var
   jsonInput: TLayeredJSONObject;
   jsonOutput: TLayeredJSONObject;
   aLabels: TLayeredJSONArray;
   t: TTodoistTask;
begin
   jsonInput := GetTaskJSON(AContent, APriority);
   try
      aLabels := TLayeredJSONArray.CreateInstance(True);
      try
         aLabels.AddString(ALabel);
         jsonInput.AddArray('labels', aLabels);
         jsonOutput := TLayeredJSONObject.CreateInstance(True);
         try
            Result := Self.ExecutePOST('https://api.todoist.com/rest/v2/tasks', jsonInput, jsonOutput);
            if Result then begin
               t.LoadFromJSONObject(jsonOutput);
               ATaskID := t.TaskID;
            end;
         finally
            jsonOutput.Free;
         end;
      finally
         aLabels.Free;
      end;
   finally
      jsonInput.Free;
   end;
end;

constructor TTodoistAPIv2.Create;
begin
   FUserAgent := 'FireflyTodoist/0.1';
   FProjects := TTodoistProjects.Create(Self);
   FProjects.FOwner := Self;
   FLabels := TTodoistLabels.Create(Self);
   FLabels.FOwner := Self;
end;

destructor TTodoistAPIv2.Destroy;
begin
   FProjects.Free;
   FLabels.Free;
   inherited Destroy;
end;

function TTodoistAPIv2.HasToken: boolean;
begin
   Result := Length(FToken) > 0;
end;

function TTodoistAPIv2.RefreshProjects: boolean;
var
   json: TLayeredJSONArray;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/projects', json);
   if Result then begin
      try
         FProjects.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

function TTodoistAPIv2.RefreshLabels: boolean;
var
   json: TLayeredJSONArray;
begin
   Result := Self.ExecuteGET('https://api.todoist.com/rest/v2/labels', json);
   if Result then begin
      try
         FLabels.LoadFromJSONArray(json);
      finally
         json.Free;
      end;
   end;
end;

constructor TTodoistDataObject.Create(AnAPI: TTodoistAPIv2);
begin
   inherited Create;
   FAPI := AnAPI;
end;

end.
