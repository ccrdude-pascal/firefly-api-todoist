{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FireflyTodoist;

{$warn 5023 off : no warning about unused units}
interface

uses
  Firefly.Todoist.ProjectPicker, Firefly.Todoist.UI, Firefly.Todoist.V2, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FireflyTodoist', @Register);
end.
