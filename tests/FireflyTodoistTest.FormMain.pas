{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick.kolla@safer-networking.org>)
   @abstract(Test form to display Todoist API data.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2023-2024 Patrick Michael Kolla-ten Venne. All rights reserved.
// License: BSD 3-Clause Revised License
// *****************************************************************************
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//      * Redistributions of source code must retain the above copyright
//        notice, this list of conditions and the following disclaimer.
//      * Redistributions in binary form must reproduce the above copyright
//        notice, this list of conditions and the following disclaimer in the
//        documentation and/or other materials provided with the distribution.
//      * Neither the name of the <organization> nor the
//        names of its contributors may be used to endorse or promote products
//        derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY Patrick Kolla-ten Venne ``AS IS'' AND ANY
//  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL Patrick Kolla-ten Venne BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2023-08-04  pk  ---  Added heade.
// *****************************************************************************
   )
}

unit FireflyTodoistTest.FormMain;

{$mode Delphi}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   ComCtrls,
   StdCtrls,
   ComboEx,
   ExtCtrls,
   Layers.JSON.Base,
   OVM.ComboBox,
   OVM.ComboBoxEx,
   OVM.TreeView,
   OVM.ListView,
   Firefly.Todoist.V2,
   Firefly.Todoist.DialogTaskSourcePicker,
   Firefly.Todoist.DialogNewTask,
   Firefly.Todoist.ControlTaskList,
   Firefly.Todoist.ControlSourcePicker;

type

   { TFormFireflyTodoistAPITest }

   TFormFireflyTodoistAPITest = class(TForm)
      bnTaskSourcePicker: TButton;
      bnAddComment: TButton;
      bnShowComments: TButton;
      bnNewTask: TButton;
      cbeTasksProjects: TComboBoxEx;
      cbTasksSections: TComboBox;
      FlowPanel1: TFlowPanel;
      FlowPanel2: TFlowPanel;
      lvProjects: TListView;
      lvLabels: TListView;
      lvSections: TListView;
      lvTasks: TListView;
      PageControl1: TPageControl;
      splitterSections: TSplitter;
      tabProjects: TTabSheet;
      tabSections: TTabSheet;
      tabLabels: TTabSheet;
      tabTasks: TTabSheet;
      tvSectionsProjects: TTreeView;
      procedure bnNewTaskClick(Sender: TObject);
      procedure bnShowCommentsClick(Sender: TObject);
      procedure bnTaskSourcePickerClick(Sender: TObject);
      procedure bnAddCommentClick(Sender: TObject);
      procedure cbeTasksProjectsSelect({%H-}Sender: TObject);
      procedure cbTasksSectionsSelect({%H-}Sender: TObject);
      procedure FormCreate({%H-}Sender: TObject);
      procedure FormDestroy({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
      procedure lvTasksSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
      procedure tvSectionsProjectsSelectionChanged({%H-}Sender: TObject);
   private
      FDemoToken: string;
      FAPI: TTodoistAPIv2;
      procedure LoadSettings;
      procedure SaveSettings;
      function CheckToken: boolean;
      procedure DoOnTodoistSelect(AnTodoistObject: TTodoistContainerObject);
   public

   end;

var
   FormFireflyTodoistAPITest: TFormFireflyTodoistAPITest;

implementation

uses
   Registry;

   {$R *.lfm}

   { TFormFireflyTodoistAPITest }

procedure TFormFireflyTodoistAPITest.FormCreate(Sender: TObject);
begin
   FAPI := TTodoistAPIv2.Create;
   FAPI.ProxyHost := '127.0.0.1';
   FAPI.ProxyPort := 8888;
end;

procedure TFormFireflyTodoistAPITest.cbeTasksProjectsSelect(Sender: TObject);
var
   p: TTodoistProject;
begin
   if cbeTasksProjects.ItemIndex < 0 then begin
      Exit;
   end;
   p := TTodoistProject(cbeTasksProjects.ItemsEx[cbeTasksProjects.ItemIndex].Data);
   lvTasks.DisplayItems<TTodoistTask>(p.Tasks);
   cbTasksSections.DisplayItems<TTodoistSection>(p.Sections);
end;

procedure TFormFireflyTodoistAPITest.bnTaskSourcePickerClick(Sender: TObject);
var
   o: TTodoistContainerObject;
begin
   if ShowTodoistTaskSourcePicker(FAPI, o) then begin
      Self.Caption := 'ShowTodoistTaskSourcePicker: ' + o.DisplayText;
   end else begin
      Self.Caption := 'ShowTodoistTaskSourcePicker: cancelled';
   end;
end;

procedure TFormFireflyTodoistAPITest.bnShowCommentsClick(Sender: TObject);
var
   t: TTodoistTask;
   c: TTodoistComment;
   s: string;
begin
   if not Assigned(lvTasks.Selected) then begin
      Exit;
   end;
   if not Assigned(lvTasks.Selected.Data) then begin
      Exit;
   end;
   t := TTodoistTask(lvTasks.Selected.Data);
   s := '';
   for c in t.Comments do begin
      s += c.Content + #13#10#13#10;
   end;
   ShowMessage(s);
end;

procedure TFormFireflyTodoistAPITest.bnNewTaskClick(Sender: TObject);
var
   json: TLayeredJSONObject;
   sID: string;
begin
   if ShowTodoistNewTaskDialog(FAPI, json) then begin
      try
         if FAPI.AddTask(sID, json) then begin
            Self.Caption := 'ShowTodoistNewTaskDialog/AddTask: ' + sID;
         end else begin
            Self.Caption := 'ShowTodoistNewTaskDialog/AddTask: failed';
         end;
      finally
         json.Free;
      end;
   end;
end;

procedure TFormFireflyTodoistAPITest.bnAddCommentClick(Sender: TObject);
var
   t: TTodoistTask;
   s: string;
begin
   if not Assigned(lvTasks.Selected) then begin
      Exit;
   end;
   if not Assigned(lvTasks.Selected.Data) then begin
      Exit;
   end;
   t := TTodoistTask(lvTasks.Selected.Data);
   if InputQuery(t.DisplayText, 'New comment:', s) then begin
      FAPI.AddTaskComment(t, s);
   end;
end;

procedure TFormFireflyTodoistAPITest.cbTasksSectionsSelect(Sender: TObject);
var
   s: TTodoistSection;
begin
   if cbTasksSections.ItemIndex < 0 then begin
      Exit;
   end;
   s := TTodoistSection(cbTasksSections.Items.Objects[cbTasksSections.ItemIndex]);
   lvTasks.DisplayItems<TTodoistTask>(s.Tasks);
end;

procedure TFormFireflyTodoistAPITest.FormDestroy(Sender: TObject);
begin
   FAPI.Free;
end;

procedure TFormFireflyTodoistAPITest.FormShow(Sender: TObject);
begin
   LoadSettings;
   if not CheckToken then begin
      Exit;
   end;
   lvLabels.DisplayItems<TTodoistLabel>(FAPI.Labels);
   lvProjects.DisplayItems<TTodoistProject>(FAPI.Projects);
   tvSectionsProjects.DisplayItems<TTodoistProject>(FAPI.Projects);
   tvSectionsProjects.FullExpand;
   cbeTasksProjects.DisplayItems<TTodoistProject>(FAPI.Projects);
end;

procedure TFormFireflyTodoistAPITest.lvTasksSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   bnAddComment.Enabled := Selected;
   bnShowComments.Enabled := Selected;
end;

procedure TFormFireflyTodoistAPITest.tvSectionsProjectsSelectionChanged(Sender: TObject);
var
   p: TTodoistProject;
begin
   p := TTodoistProject(tvSectionsProjects.Selected.Data);
   lvSections.DisplayItems<TTodoistSection>(p.Sections);
end;

procedure TFormFireflyTodoistAPITest.LoadSettings;
var
   reg: TRegistry;
begin
   reg := TRegistry.Create;
   try
      reg.RootKey := HKEY_CURRENT_USER;
      if reg.OpenKey('\Software\Firefly\Todoist\', True) then begin
         try
            if reg.ValueExists('DemoToken') then begin
               Self.FDemoToken := reg.ReadString('DemoToken');
            end;
         finally
            reg.CloseKey;
         end;
      end;
   finally
      reg.Free;
   end;
end;

procedure TFormFireflyTodoistAPITest.SaveSettings;
var
   reg: TRegistry;
begin
   reg := TRegistry.Create;
   try
      reg.RootKey := HKEY_CURRENT_USER;
      if reg.OpenKey('\Software\Firefly\Todoist\', True) then begin
         try
            reg.WriteString('DemoToken', Self.FDemoToken);
         finally
            reg.CloseKey;
         end;
      end;
   finally
      reg.Free;
   end;
end;

function TFormFireflyTodoistAPITest.CheckToken: boolean;
begin
   Result := False;
   if Length(Self.FDemoToken) = 0 then begin
      if not InputQuery('Todoist Login', 'Please enter token:', FDemoToken) then begin
         Exit;
      end;
      SaveSettings;
   end;
   if Length(Self.FDemoToken) = 0 then begin
      Exit;
   end;
   FAPI.Token := FDemoToken;
   Result := True;
end;

procedure TFormFireflyTodoistAPITest.DoOnTodoistSelect(AnTodoistObject: TTodoistContainerObject);
begin

end;

end.
